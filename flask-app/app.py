from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
    "https://media.tenor.com/images/5e43669b9573fea08ede20a5bbbbe2df/tenor.gif",
    "https://media.tenor.com/images/d12f9890d49ea8a68664115a68585545/tenor.gif",
    "https://media.tenor.com/images/7e8c225f085620232321ff16d6c09b96/tenor.gif",
    "https://media.tenor.com/images/43658eb1d5a373d22e7634a0a76d1625/tenor.gif",
    "https://media.tenor.com/images/d98e76e3930cf171cc39e301c9e974af/tenor.gif",
    "https://media.tenor.com/images/35c9d45c9778e8019dff3efe3bb0223f/tenor.gif",
    "https://media.tenor.com/images/05d5a94ad268646d05d8862f2a3fe7ef/tenor.gif",
    "https://media.tenor.com/images/6af4970ecc7964ecb8faee075455a227/tenor.gif",
    "https://media.tenor.com/images/3372100a31355e9173411912d0ed845e/tenor.gif",
    "https://media.tenor.com/images/4c0b3af71cf05828c26f6ad946fa373b/tenor.gif",
    "https://media.tenor.com/images/b1cfad3361af04a2147d4aa3f276b6d7/tenor.gif"
]


@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)


if __name__ == "__main__":
    app.run(host="0.0.0.0")
