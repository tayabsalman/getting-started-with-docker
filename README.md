# This Repo is to keep all the getting started with docker in one place.

## Installing Docker by refereing to the docs

link: https://docs.docker.com/engine/install/ubuntu/

### Post installation steps(optional)

Docker runs as root user, so every command must be prefixed with sudo.
so we need to manage Docker as non-root user

To create the docker group and add your user:

1. Create a new user group (Docker might have created this for you)

```
$ sudo groupadd docker
```

2.Add your user to the above group

```
$ sudo usermod -aG docker \$USER
```

3 Logout current user
(In my case logout does not seem to apply the new previlages, so I rebooted the system)

### Getting familiar with Docker by going through the docker samples and tutorials

link: https://docs.docker.com/samples/
